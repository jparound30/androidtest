/*
 * Copyright (C) 2008 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.example.android.skeletonapp;

import android.test.ActivityInstrumentationTestCase2;
import android.test.ActivityUnitTestCase;
import android.test.TouchUtils;
import android.view.KeyEvent;
import android.widget.EditText;
import android.widget.Button;
import android.app.Activity;
/**
 * Make sure that the main launcher activity opens up properly, which will be
 * verified by {@link #testActivityTestCaseSetUpProperly}.
 */
public class SkeletonAppTest extends ActivityInstrumentationTestCase2<SkeletonActivity> {

    /**
     * Creates an {@link ActivityInstrumentationTestCase2} for the {@link SkeletonActivity}
     * activity.
     */
    public SkeletonAppTest() {
        super(SkeletonActivity.class);
    }

    /**
     * Verifies that the activity under test can be launched.
     */
    public void testActivityTestCaseSetUpProperly() {
        assertNotNull("Activityが正常に起動していることかのチェック", getActivity());
    }

    public void testKeyHandle1() {
    	Activity activity = getActivity();
        final Button button = (Button)activity.findViewById(com.example.android.skeletonapp.R.id.clear);
        final EditText edit = (EditText)activity.findViewById(com.example.android.skeletonapp.R.id.editor);

        /*
         * clearボタンを押下
         */
        TouchUtils.clickView(this, button);

        assertEquals("Clearボタンがクリックされた時の値のチェック", "", edit.getText().toString());
    }

    public void testMenuClear() {
    	Activity activity = getActivity();
        final EditText edit = (EditText)activity.findViewById(com.example.android.skeletonapp.R.id.editor);

        /*
         * Menuボタンを押下
         */
        sendKeys(KeyEvent.KEYCODE_MENU);
        /*
         * Clearを選択(ショートカットキーの：Cで実行）
         * その他にいい手段が思いつかないので。
         */
    	sendKeys(KeyEvent.KEYCODE_C);

    	assertEquals("Menu->clearがクリックされた時の値のチェック", "", edit.getText().toString());
    }

    public void testMenuBack() {
    	Activity activity = getActivity();

        /*
         * Menuボタンを押下
         */
    	sendKeys(KeyEvent.KEYCODE_MENU);
        /*
         * Backを選択(ショートカットキーの：Bで実行）
         * その他にいい手段が思いつかないので。
         */
    	sendKeys(KeyEvent.KEYCODE_B);

    	assertTrue("finish()が呼ばれているかのチェック", activity.isFinishing());
    }
}
