package com.example.android.skeletonapp;

import com.example.android.skeletonapp.SkeletonActivity;

import android.test.ActivityUnitTestCase;
import android.test.TouchUtils;
import android.view.KeyEvent;
import android.widget.EditText;
import android.widget.Button;
import android.app.Activity;
import android.content.Intent;

public class SkeletonActivityTest extends ActivityUnitTestCase<SkeletonActivity> {
    public SkeletonActivityTest() {
        super(SkeletonActivity.class);
    }
    public void testBack() {
    	/*
    	 *  テスト対象のActivityをkickする
    	 */
        startActivity(new Intent(), null, null);
        SkeletonActivity activity = getActivity();

        final Button button = (Button)activity.findViewById(com.example.android.skeletonapp.R.id.back);

        /*
         * TouchUtilsクラスは、ActivityUnitTestCaseでは使えないっぽい。
         * INJECT_EVENTSのpermissionがないという例外が出て実行出来ない。
         * 代わりにrunOnUiThreadでボタン押下などをやる。
         */
        //TouchUtils.clickView(this, button);
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                button.performClick();
            }
        });
        getInstrumentation().waitForIdleSync();
        
        /*
         *  Activityが終了している(finish()が呼ばれている）ことを確認
         */
    	assertTrue("activity should be finished", isFinishCalled());
    }
}
